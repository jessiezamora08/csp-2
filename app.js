// import modules 

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors   = require('cors')
let port = process.env.PORT || 5000;
const database_url ="mongodb+srv://admin:1234@csp2.sax1r.mongodb.net/E-commerce?retryWrites=true&w=majority"


//middlewares 
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//connection to database 
mongoose.connect(database_url, { useNewUrlParser: true,
useUnifiedTopology: true})
const db = mongoose.connection;
db.on('error', console.error.bind(console,'Cannot connect to database'));
db.once('open', () => {console.log(`Connected to database`)})


//connecting Routes module 
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
// entry point URL 
app.use('/users',userRoutes);
app.use ('/products',productRoutes);

//server --- do not erase
app.listen(port, () => {console.log(`server is running at ${port}`)})

