const Product = require("../models/Product");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports = {
  addProduct: async (userData, reqBody) => {
    const { name, description, price, orders } = await reqBody;

    if (userData.isAdmin == true) {
      const newProduct = new Product({
        name,
        description,
        price,
        orders,
      });

      return newProduct.save().then((res, rej) => {
        if (res) {
          return { message: "Product Added", productDetails: reqBody };
        }
        return { message: "Error Occurred" };
      });
    }
    return { message: "Access Denied" };
  },
  getAll: () => {
    return Product.find().then((res, rej) => {
      if (res) {
        return res;
      }
      return { message: "Error occurred" };
    });
  },
  getAllActive: () => {
    return Product.find().then((res, rej) => {
      if (res) {
        let activeProduct = [];
        const total = res.length;

        for (let i = 0; i < total; i++) {
          if (res[i].isActive == true) {
            activeProduct.push(res[i]);
          }
        }
        return activeProduct;
      }
      return { message: "Error occurred." };
    });
  },
  getProduct: async (id) => {
    const search = await Product.findById(id);
    if (search == null) {
      return { message: "Something went wrong" };
    }
    return Product.findById(id);
  },
  updateProduct: async (userData, id, reqBody) => {
    const { name, description, price } = await reqBody;

    const search = await Product.findById(id);
    if (search == null) {
      return { message: "Something went wrong" };
    }
    if (userData.isAdmin == true) {
      const product = await Product.findByIdAndUpdate(id, {
        name,
        description,
        price,
      });

      return product;
    }
    return { message: "Access Denied" };
  },
  archiveProduct: async (userData, id) => {
    const search = await Product.findById(id);
    if (search == null) {
      return { message: "Something went wrong" };
    }
    if (userData.isAdmin == true) {
      return Product.findByIdAndUpdate(id, {
        isActive: false,
      }).then((res) => {
        return { message: "Product now on archive" };
      });
    }
    return { message: "Access Denied" };
  },
  activateProduct: async (userData, id) => {
    const search = await Product.findById(id);
    if (search == null) {
      return { message: "Something went wrong" };
    }

    if (userData.isAdmin == true) {
      return await Product.findByIdAndUpdate(id, {
        isActive: true,
      }).then((res) => {
        return { message: "Product now Activated" };
      });
    }
    return { message: "Access Denied" };
  },
};
