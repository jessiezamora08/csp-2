// import model

const User = require("../models/User");
const auth = require("../auth");
const bcrypt = require("bcrypt");
const Product = require("../models/Product");
const { findByIdAndUpdate } = require("../models/User");

module.exports = {
  //REGISTER USER :

  register: (reqBody) => {
    const { email, password } = reqBody;

    const newUser = new User({
      email,
      password: bcrypt.hashSync(password, 10)
    });

    return newUser
      .save()
      .then((res, rej) => {
        if (res) {
          return {
            message: "Sucessfully Registered!",
            email,
          };
        }
        return {
          error: "Oops. Something went wrong.",
        };
      })
      .catch((err) => {
        console.log(err);
      });
  },

  //LOGIN
  login: (reqBody) => {
    const { email, password } = reqBody;

    return User.findOne({ email })
      .then((res) => {
        if (res == null) {
          return { message: "Email not found!" };
        } else {
          const inputPass = bcrypt.compareSync(password, res.password)
          if (inputPass == true) {
            return {
              access: auth.createAccessToken(res),
            };
          }
          return {
            message: "Password is incorrect",
          };
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },

  //CHECKOUT
  checkout: async (userData, cart) => {
    // for user only
    if (userData.isAdmin == false) {
      // to add a new order sa database

      const newOrder = await User.findByIdAndUpdate(
        userData.id,
        { $addToSet: { orders: cart } },
        { new: true }
      );
        
      //get the latest order

      const latestOrder = newOrder.orders[newOrder.orders.length - 1];
      const orderId = latestOrder._id;

  
      let productId;
      let productDetail;

      for (let i = 0; i < cart.products.length; i++) {
        productId = cart.products[i].productId;

        productDetail = await Product.findByIdAndUpdate(
          productId,
          { $addToSet: { orders: [{ orderId }] } },
          { new: true }
        );
      }

      return {
        "productDetails": productDetail,
        "latestOrder": latestOrder
      }
    }
    return { message: "Admin cannot perform this task" };
  },

  //MY ORDERS

  getMyOrders: async (userData) => {
    if (userData.isAdmin == false) {
      const myOrders = await User.findById(userData.id);
      return myOrders.orders;
    }
    return { message: "Admin cannot perform this task" };
  },

  //VIEW ALL ORDERS AS ADMIN

  getAllOrders: async (userData) => {
    if (userData.isAdmin == true) {
      const allOrders = await User.find();
      let getOrders = [];

      for (let i = 0; i < allOrders.length; i++) {
        if (allOrders[i].orders.length >= 1) {
          getOrders.push({
            email: allOrders[i].email,
            userId: allOrders[i]._id,
            orders: allOrders[i].orders,
          });
        }
      }
      return getOrders;
    }
    return { message: "Access Denied" };
  },
};
