// import modules
const jwt =require('jsonwebtoken');
const secret = "Kitty";

module.exports = {
    createAccessToken: (user) => {
        const data = {
            id: user._id, 
            email: user.email,   
            isAdmin: user.isAdmin  
        }

        return jwt.sign(data,secret);
    },   

    decode: (token) => {
        let slicedToken = token.slice(7, token.length);
        return jwt.decode(slicedToken)
    },

    verify: (req,res, next) => {
        let token = req.headers.authorization;

        if(typeof token !== "undefined"){
            let slicedToken = token.slice(7, token.length)

            return jwt.verify(slicedToken,secret, (err,data) =>{
                if (err) {
                    res.send ({auth: "authentication failed"})
                }next();
            })
        }res.send({message: "undefined token"})
    }
}