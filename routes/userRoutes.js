//import modules
const { Router } = require('express');
const express = require('express');
const userController = require('../controllers/userControllers');
const router = express.Router();

const auth = require("./../auth")

// REGISTER 
router.post('/register',(req,res) => {
    userController.register(req.body).then(result => {
        res.send(result)
    })
})

//LOG IN
router.post('/login', (req,res) => {
    userController.login(req.body)
    .then(result => {res.send(result)})
})

//CHECKOUT 
router.post("/checkout", auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)

    userController.checkout(userData, req.body)
    .then(result => {
        res.send(result.latestOrder)
    })
})

//My ORDERS
router.get('/my-orders', auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)

    userController.getMyOrders(userData, req.body)
    .then(result => {
        res.send(result)
    })

})

//View all orders (Admin)
router.get('/orders', auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)

    userController.getAllOrders(userData, req.body)
    .then(result => {
        res.send(result)
    })
})



module.exports = router