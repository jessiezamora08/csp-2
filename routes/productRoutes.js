const express = require('express');
const productController = require('../controllers/productControllers');
const router = express.Router();

const auth = require("./../auth")



//ADD PRODUCTS (ADMIN ONLY)
router.post('/',auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)

    productController.addProduct(userData, req.body)
        .then(result => {
            res.send(result)
        })

})


//GET ALL PRODUCTS 

router.get('/all', (req,res) => {
    productController.getAll()
    .then(resultFromController => res.send(resultFromController))
})

// GET ALL ACTIVE PRODUCTS 

router.get('/active',(req,res) => {
    productController.getAllActive()
    .then(result => {
        res.send(result)
    })
})


//GET SPECIFIC PRODUCT 

router.get("/:productId", (req,res) => {
    productController.getProduct(req.params.productId)
    .then(result => {
        res.send(result)
    })
})


//UPDATE PRODUCT 
router.put("/:productId", auth.verify, (req,res) => {
    
    let userData = auth.decode(req.headers.authorization)

    productController.updateProduct(userData, req.params.productId, req.body)
    .then(result => {
        res.send(result)
    })
})


//ARCHIVE PRODUCT - set isActive to false 

router.put ('/:productId/archive', auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)
    
    productController.archiveProduct(userData, req.params.productId)
        .then(result => {
            res.send(result)
        })
})

//UNARCHIVE or ACTIVATE PRODUCT - set isActive to true from false 

router.put('/:productId/activate', auth.verify, (req,res) => {
    let userData = auth.decode(req.headers.authorization)
    
    productController.activateProduct(userData, req.params.productId)
        .then(result => {
            res.send(result)
        })
})



module.exports= router
